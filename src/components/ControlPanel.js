import React from "react";

function creatRandomAnswer() {
    let result = [];
    let ranNum;
    for (let i = 0; i < 4; i++) {
        ranNum = Math.ceil(Math.random() * 25);
    }
    result.push(String.fromCharCode(65 + ranNum));
    return result.toString();
}

class ControlPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            answer: '',
            input1: '',
            input2: '',
            input3: '',
            input4: '',
            newCard: false,
            showResult: false,
        }
    }

    getSubAnswer(pre,length){
        return this.state.answer.substr(pre, length);
    }

    handleNewCard() {
        const answer  = creatRandomAnswer();
        this.props.setAnswer(answer);
        this.setState({
            newCard: true,
            answer:answer,
        });
        this.props.setNewCard(true);

        this.setState({
            input1:this.getSubAnswer(0,1),
            input2:this.getSubAnswer(1,1),
            input3:this.getSubAnswer(2,1),
            input4:this.getSubAnswer(3,1),
        })

        setInterval(()=>{
            this.setState({
                input1:'',
                input2:'',
                input3:'',
                input4:'',
                newCard:false,
            });
            this.props.setNewCard(false);
        },3000)
    }

    handleShowResult(){
        this.setState({
            showResult:true,
            input1:this.getSubAnswer(0,1),
            input2:this.getSubAnswer(1,1),
            input3:this.getSubAnswer(2,1),
            input4:this.getSubAnswer(3,1),
        })
        this.props.setShowResult(true);
    }

    render() {
        return (
            <div className="control-panel">
                <h1>New Card</h1>
                <button className="new-game" onClick={this.handleNewCard.bind(this)}>New Card</button>
                <div className="control-input">
                    <input type="text" value={this.state.input1}/>
                    <input type="text" value={this.state.input2}/>
                    <input type="text" value={this.state.input3}/>
                    <input type="text" value={this.state.input4}/>
                </div>
                <button  className="show-result" onClick={this.handleShowResult.bind(this)}>Show Result</button>
            </div>
        )
    }
}

export default ControlPanel;
