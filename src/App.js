import React from "react";
import ControlPanel from "./components/ControlPanel";
import PlayingPanel from "./components/PlayingPanel";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            answer: '',
            newCard: false,
            showResult: false,
        }
        this.setAnswer=this.setAnswer.bind(this);
        this.setNewCard=this.setNewCard.bind(this);
        this.setShowResult=this.setShowResult.bind(this);
    }
    setAnswer(answer){
        this.setState({
            answer:answer,
        });
    }
    setNewCard(status){
        this.setState({
            newCard: status,
        });
    }
    setShowResult(status){
        this.setState({
            showResult: status,
        });
    }

    render() {
        return (
            <div className="main">
                <PlayingPanel newCard ={this.state.newCard} showResult={this.state.showResult}/>
                <ControlPanel setAnswer = {this.setAnswer}
                              setShowResult = {this.setShowResult} setNewCard={this.setNewCard}/>
            </div>
        )
    }

}

export default App;
