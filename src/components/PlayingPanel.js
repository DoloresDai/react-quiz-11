import ControlPanel from "./ControlPanel";
import React from "react";

class PlayingPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            guess: '',
            correct: false,
            input1: '',
            input2: '',
            input3: '',
            input4: '',
        }
    }

    getSubGuess(pre, length) {
        return this.state.guess.substr(pre, length);
    }

    handleGuessInput(e) {
        this.setState({
            guess: e.target.value,
        });
        this.handleGuessInputSub();
    }

    handleGuessInputSub() {
        switch (this.state.guess.length) {
            case 1:
                this.setState({
                    input1: this.getSubGuess(1, 1),
                });
                break;
            case 2:
                this.setState({
                    input1: this.getSubGuess(0, 1),
                    input2: this.getSubGuess(1, 1),
                });
                break;
            case 3:
                this.setState({
                    input1: this.getSubGuess(0, 1),
                    input2: this.getSubGuess(1, 1),
                    input3: this.getSubGuess(2, 1),
                });
                break;
            case 4:
                this.setState({
                    input1: this.getSubGuess(0, 1),
                    input2: this.getSubGuess(1, 1),
                    input3: this.getSubGuess(2, 1),
                    input4: this.getSubGuess(3, 1),
                });

                break;
        }
    }

    handleGuessButton() {
        if (this.props.state.answer === this.state.guess) {
            this.setState({
                correct: true,
            })
        }
    }


    render() {
        return (
            <div className="play-panel">
                <div className="result">
                    <h1>Your Result</h1>
                    <div className="result-input">
                        <input type="text" value={this.state.input1}/>
                        <input type="text" value={this.state.input2}/>
                        <input type="text" value={this.state.input3}/>
                        <input type="text" value={this.state.input4}/>
                    </div>
                    {this.props.newCard || <h2 className="result">{this.state.correct ? "SUCCESS" : "FAILED"}</h2>
                    || this.props.showResult || <h2 className="result">{this.state.correct ? "SUCCESS" : "FAILED"}</h2>}
                </div>
                <div className="guess">
                    <h1 className="guess-card">Guess Card</h1>
                    <input type="text" className="guess-input"
                           onChange={this.handleGuessInput.bind(this)} value={this.state.guess}/>
                    <button className="guess-button" onClick={this.handleGuessButton.bind(this)}>Guess</button>
                </div>
            </div>

        )
    }
}

export default PlayingPanel;
